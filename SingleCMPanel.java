/*
 * SingleCMPanel.java
 *
 * Created on June 15, 2007, 12:16 PM
 */

package CSApackage;

import static CSApackage.V_to_mu_plot.dashed;
import java.awt.*;
import java.text.NumberFormat;

/**
 *
 * @author  Owner
 */
public class SingleCMPanel extends javax.swing.JPanel
{
  private Mac theMac = null;
  V_to_mu_plot m_Plot_Panel = null;
  
  int mouse_X;
  int mouse_Y;
  
  int cellHorizInset = 3;
  int cellDiameter = 14;
  int cellHorizSpace = cellDiameter + 2 * cellHorizInset;
  
  int num_Y_AxisTicks = 5;
  
  float[] V_vals = null;
  float[] muVals = null;
  
  int m_vert_spacer = 35;
  
  int plotHeightPixels = 0;
  int overallHeight = 0;
  
  int plotWidthPixels = 500;
  int plotOrigin_X_Inset = 140;
  int x_AxisRightBufferPixels = 20;
  int overallWidth = plotOrigin_X_Inset + plotWidthPixels + x_AxisRightBufferPixels;
  
  int m_p_plot_height = 0;
  int m_p_plot_bottom = 0;
  int m_p_plot_y_mid = 0;
  
  int m_V_plot_height = 0;
  int m_V_plot_bottom = 0;  
  int m_V_plot_y_mid = 0;
  
  Color incorrectLossColor = new Color(255, 153, 153); // Color.red;    
  
  int m_cell_top = 0;
  
  public NumberFormat m_FloatFormat = NumberFormat.getNumberInstance();
  public NumberFormat m_FloatFormat_prob = NumberFormat.getNumberInstance();
  
  MainCSA_demoPanel m_Controller = null;
  
  /**
   * Creates new form single CM plan panel
   * 
   */
  public SingleCMPanel()
  {
    m_FloatFormat.setMaximumFractionDigits(1);
    m_FloatFormat.setMinimumFractionDigits(1);
    m_FloatFormat.setMinimumIntegerDigits(1);
    
    m_FloatFormat_prob.setMaximumFractionDigits(2);
    m_FloatFormat_prob.setMinimumFractionDigits(2);
    m_FloatFormat_prob.setMinimumIntegerDigits(1);
    
    initComponents();
  }
  
  /** This method is called from within the constructor to
   * initialize the form.
   * WARNING: Do NOT modify this code. The content of this method is
   * always regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
      public void mouseMoved(java.awt.event.MouseEvent evt) {
        formMouseMoved(evt);
      }
    });
    setLayout(null);
  }// </editor-fold>//GEN-END:initComponents

  private void formMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseMoved
    // TODO add your handling code here:
    mouse_X = evt.getX();
    mouse_Y = evt.getY();  
    repaint();
  }//GEN-LAST:event_formMouseMoved
  
  public void SetController ( MainCSA_demoPanel controller )
  {
    m_Controller = controller;
  }
  
  public void set_plot_class( V_to_mu_plot plotPanel ) { m_Plot_Panel = plotPanel; }
  
  Font axisValuesFont = new Font("Serif", Font.BOLD, 14);
  Font axisVarFont = new Font("Serif", Font.BOLD | Font.ITALIC, 20);
  Font axisVarFontLarge = new Font("Serif", Font.BOLD | Font.ITALIC, 26);
  Font titleFont = new Font("Serif", Font.BOLD, 24);
          
  protected void paintComponent(java.awt.Graphics g)
  {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    
    overallHeight = this.getHeight();
    overallWidth = this.getWidth();
    plotWidthPixels = overallWidth - plotOrigin_X_Inset - x_AxisRightBufferPixels;    
    
    if ( m_Plot_Panel == null ) { return; }
    
    m_p_plot_height = (int) ( overallHeight * 0.35 );
    m_V_plot_height = m_p_plot_height;
    
    m_p_plot_bottom = m_p_plot_height + m_vert_spacer; 
    m_V_plot_bottom = m_p_plot_bottom + m_V_plot_height + m_vert_spacer; 
    
    m_p_plot_y_mid = (int) ( (float) m_p_plot_bottom - m_p_plot_height / 2 );
    m_V_plot_y_mid = (int) ( (float) m_V_plot_bottom - m_V_plot_height / 2 );
    
    m_cell_top = m_V_plot_bottom + 10;
    
    // Draw background of chart in same color as background of first CM in mac panel chart   
    if (m_Controller != null)
      g2.setColor(m_Controller.first_CM_background);
    g2.fillRect(plotOrigin_X_Inset, m_vert_spacer, getWidth() - plotOrigin_X_Inset - x_AxisRightBufferPixels, m_p_plot_height);
    g2.fillRect(plotOrigin_X_Inset, m_p_plot_bottom + m_vert_spacer, getWidth() - plotOrigin_X_Inset - x_AxisRightBufferPixels, m_V_plot_height);
    
    g2.setColor( Color.BLACK );
    
    int y = 0;    
    int numCells = theMac.K;
    
    // compute how wide bars should be to fit everything
    cellHorizSpace = (numCells > 0) ? plotWidthPixels / numCells : cellHorizSpace;
    if (cellHorizSpace > 20)
    {
      cellHorizInset = 3;
      cellDiameter = 14;
      cellHorizSpace = cellDiameter + 2 * cellHorizInset;
    }
    else
    {
      cellHorizInset = (int)(cellHorizSpace/7.0);
      cellDiameter = cellHorizSpace - 2 * cellHorizInset;
    }    
    
    // draw x-axes for rho and V plots   
    
    g2.drawLine(plotOrigin_X_Inset, m_p_plot_bottom, getWidth() - x_AxisRightBufferPixels, m_p_plot_bottom );
    g2.setFont(axisVarFontLarge);
    g2.drawString("\u03c1", (int) (plotOrigin_X_Inset / 3) - 3, m_p_plot_y_mid - 32);
    g2.setFont(axisVarFont);
    g2.drawString( "Prob. of", 10, m_p_plot_y_mid);
    g2.drawString( "Winning", 10, m_p_plot_y_mid + 22 );
    g2.drawString( "(normed \u03bc)", 10, m_p_plot_y_mid + 44 );
    
    g2.drawLine(plotOrigin_X_Inset, m_V_plot_bottom, getWidth() - x_AxisRightBufferPixels, m_V_plot_bottom );
    g2.setFont(axisVarFontLarge);
    g2.drawString("V", (plotOrigin_X_Inset / 3) - 3, m_V_plot_y_mid - 30 );
    g2.setFont(axisVarFont);
    g2.drawString( "Normed", 10, m_V_plot_y_mid - 4);
    g2.drawString( "Synaptic", 10, m_V_plot_y_mid + 18);
    g2.drawString( "Support", 10, m_V_plot_y_mid + 40 );
    
    // draw y-axes for rho and V plots
    
    g2.drawLine(plotOrigin_X_Inset, m_p_plot_bottom, plotOrigin_X_Inset, m_p_plot_bottom - m_p_plot_height );
    g2.drawLine(plotOrigin_X_Inset, m_V_plot_bottom, plotOrigin_X_Inset, m_V_plot_bottom - m_V_plot_height );
    
    g2.setFont(axisValuesFont);
    int y_axis_maj_interval = (int) ( m_p_plot_height / (float) num_Y_AxisTicks );
    int y_pos = 0;
    for (int h = 0; h <= num_Y_AxisTicks; h++)                                              // draw y-axis ticks and vals
    {
      y_pos = h * y_axis_maj_interval;
      g2.drawString(m_FloatFormat.format((float) h / num_Y_AxisTicks ), plotOrigin_X_Inset - 22, m_p_plot_bottom - y_pos + 6 );
      g2.drawString(m_FloatFormat.format((float) h / num_Y_AxisTicks ), plotOrigin_X_Inset - 22, m_V_plot_bottom - y_pos + 6 );      
    }
    
    g2.drawString("Cell", 30, m_cell_top + 15);
    
    // draw cells and bars
    
    int winnerIndex = theMac.getWinningIndex(0);
    int max_V_Index = theMac.getMax_V_Index(0);      
    
    double divisor = ( theMac.muSum.get(0) > 0 ) ?  theMac.muSum.get(0) : 1;
      
    int x_left = 0;  // convenience var in loop    
    for (int c = 0; c < numCells; c++)
    {      
      if (c == max_V_Index && c != winnerIndex )
        g2.setColor( incorrectLossColor );
      else if (c == winnerIndex)
        g2.setColor( Color.black );
      else
        g2.setColor( Color.lightGray );    
      
      x_left = plotOrigin_X_Inset + c * cellHorizSpace + cellHorizInset;
      
      g2.fillOval(x_left, m_cell_top, cellDiameter, cellDiameter );
      
      y = (int) ( theMac.get_specific_V_val(0, c) * m_V_plot_height );
      g2.fillRect(x_left, m_V_plot_bottom - y, cellDiameter, y );
      
      y = (int) ( theMac.get_specific_mu_val(0, c) / divisor * m_p_plot_height );
      g2.fillRect(x_left, m_p_plot_bottom - y, cellDiameter, y );
      
      // show values as mouse hovers over bars
      if (mouse_X >= x_left && mouse_X <= x_left + cellDiameter) // && mouse_Y >= y_pos-3 && mouse_Y <= y_pos+3 )
      {
        g2.setColor( Color.black );
        g2.drawString(m_FloatFormat_prob.format(theMac.get_specific_V_val(0, c)), x_left, m_V_plot_bottom - m_V_plot_height- 12 ); 
        g2.drawString(m_FloatFormat_prob.format(theMac.get_specific_mu_val(0, c) / theMac.muSum.get(0)), x_left - 8, m_p_plot_bottom - m_p_plot_height - 12 ); 
      }
      
    }
    
     // Draw faint horizontal lines to show crosstalk limits.
    int y_max_V_in_pixels = 0;
    if (m_Controller != null)
    {
      g2.setColor( Color.lightGray );
      Stroke oldStroke = g2.getStroke();
      g2.setStroke(dashed);

      if (m_Controller.isCrossTalkRelativeToCurrentMax_V())
        y_max_V_in_pixels = (int) (m_V_plot_height * theMac.GetWinner_V_Val());
      else
        y_max_V_in_pixels = (int) (m_V_plot_height * 1);
      
      int min_crosstalk_y = m_V_plot_bottom - (int)(theMac.GetCrossTalkLowLimFactor() * y_max_V_in_pixels);
      g2.drawLine(plotOrigin_X_Inset, min_crosstalk_y, getWidth() - x_AxisRightBufferPixels, min_crosstalk_y );
      int max_crosstalk_y = m_V_plot_bottom - (int)(theMac.GetCrossTalkHighLimFactor() * y_max_V_in_pixels);
      g2.drawLine(plotOrigin_X_Inset, max_crosstalk_y, getWidth() - x_AxisRightBufferPixels, max_crosstalk_y ); 
      g2.setStroke(oldStroke);
    }
    
  }

  /**
   * @return the theMac
   */
  public Mac getTheMac() {
    return theMac;
  }

  /**
   * @param theMac the theMac to set
   */
  public void setTheMac(Mac theMac) {
    this.theMac = theMac;
  }
  
  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables
  
}
